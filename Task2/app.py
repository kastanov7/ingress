from flask import Flask
import datetime

app = Flask(__name__)

@app.route('/date')
def get_date():
    current_time = datetime.datetime.now()
    seconds = current_time.second

    if seconds % 2 == 0:
        return f"{current_time.strftime('%H:%M:%S')} EVEN\n"
    else:
        return f"{current_time.strftime('%H:%M:%S')} ODD\n"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)



